package com.epam.librarys;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class dataHad{
    private ArrayList<String> elem = new ArrayList<String>();
    public void elementsLinks(String input){
        //String input = "Input text, with words, punctuation, etc. Well, it's rather short.";
        Pattern p = Pattern.compile("[\\w']+");
        Matcher m = p.matcher(input);

        while ( m.find() ) {
            elem.add(input.substring(m.start(), m.end()).toLowerCase());
        }

    }

    public void search(){
        String min="";
        String max="";
        int i=1;
        for (String anElem : elem) {
            if(i==elem.size()){
                break;
            } else if (anElem.length() <= elem.get(i).length()) {
                min = anElem;
                max = elem.get(i);
            } else {
                min = elem.get(i);
                max = anElem;
            }
            i++;
        }

        System.out.println("min: "+min + " | max: " + max);
    }
}
